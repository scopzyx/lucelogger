/*
 * m_usb_filter_list.h
 *
 *  Created on: 6Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_M_USB_FILTER_LIST_H_
#define USER_INC_M_USB_FILTER_LIST_H_

#define MAX_CMD_SIZE 32
typedef struct cmdItem_t
{
	uint8_t bytes[MAX_CMD_SIZE];
	uint16_t size;
}cmdItem_t;

typedef struct filterItem_t
{
	cmdItem_t txItem;
	cmdItem_t rxItem;
}filterItem_t;


const filterItem_t filterList[] =
{
		//Startup Command?
		{
			.rxItem =
			{
					{0x01, 0x02, 0xFC},
					3
			},
			.txItem =
			{
					{ 0x01, 0x03, 0x01, 0xFA },
					4
			}
		},
		//Power FC Model.
		{
			.rxItem =
			{
					{0xF3, 0x02, 0x0A},
					0x03
			},
			.txItem =
			{
					{ 0xF3, 0x10, 0x53, 0x52,0x32,0x30,0x44,0x45,0x54,0x4F,0x4E,0x41,0x54,0x49,0x4F,0x4E,0x00},
					0x11
			}
		},
	    // Version number
		{
			.rxItem =
			{
					{0xF5,0x02,0x08},
					0x03
			},
			.txItem =
			{
					{0xF5,0x07,0x36,0x2E,0x39,0x39,0x20,0x0D},
					0x08
			}
		}
};
#endif /* USER_INC_M_USB_FILTER_LIST_H_ */
