/**
*	@file:		m_trace_api.h
*	@project:	
*	@brief:
*	@note:
*	@date:		27Mar.,2020
*	@version:	1.0
*	@author:	Alex Williams
*	@attention: 
*
*/

#ifndef MRD_INC_M_TRACE_API_H_
#define MRD_INC_M_TRACE_API_H_
#include "stdint.h"
#include "FreeRTOS.h"

#define LOG(format, ...)  vTrace_Log(format, __FILE__, __LINE__, ##__VA_ARGS__)

BaseType_t xTrace_Init(UBaseType_t uxQueueLength );
void vTrace_Process(void);
void vTrace_EchoProcess(void);
void vTrace_Log(const char* fmt, const char* file, int line, ...);
void vTrace_FailurePrint(void);
void vTrace_LedOff(void);
void vTrace_LedOn(void);

#endif /* MRD_INC_M_TRACE_API_H_ */
