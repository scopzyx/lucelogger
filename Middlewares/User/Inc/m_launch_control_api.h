/*
 * m_launch_control.h
 *
 *  Created on: 4Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_M_LAUNCH_CONTROL_API_H_
#define USER_INC_M_LAUNCH_CONTROL_API_H_

#include "FreeRTOS.h"

typedef struct launchData_t
{
	uint16_t RPM;
	uint16_t Speed;
}launchData_t;

BaseType_t xLaunchControl_Init(void);
BaseType_t xLaunchControl_IsActive();
BaseType_t xLaunchControl_GetData(launchData_t * data);
BaseType_t xLaunchControl_KillIgnition(uint8_t killTime);

#endif /* USER_INC_M_LAUNCH_CONTROL_API_H_ */
