/*
 * m_usb_api.h
 *
 *  Created on: 4Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_M_USB_CONTROL_API_H_
#define USER_INC_M_USB_CONTROL_API_H_
#include "FreeRTOS.h"
BaseType_t xUsbControl_Init();
void vUsbControl_SendRecv(uint32_t timeout);
BaseType_t xUsbControl_IsActive(void);

#endif /* USER_INC_M_USB_CONTROL_API_H_ */
