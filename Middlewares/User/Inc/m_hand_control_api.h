/*
 * m_apex_echo_api.h
 *
 *  Created on: 4Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_M_HAND_CONTROL_API_H_
#define USER_INC_M_HAND_CONTROL_API_H_
#include "FreeRTOS.h"

void vHandControl_Enable(void);
void vHandControl_Disable(void);
BaseType_t xHandControl_Init(void);

#endif /* USER_INC_M_HAND_CONTROL_API_H_ */
