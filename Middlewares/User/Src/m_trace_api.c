/**
*	@file:		M_trace_api.c
*	@project:	
*	@brief:
*	@note:
*	@date:		27Mar.,2020
*	@version:	1.0
*	@author:	Alex Williams
*	@attention: 
*
*/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "d_debug.h"
#include "task.h"
#include "m_trace_api.h"

#define LOG_BUFFER_SIZE	    256
#define LOG_SIZE		    128
#define TRACE_TIMEOUT	 	500

const char traceCriticalMessage[] = "DEBUG CRITICAL ERROR.";

static QueueHandle_t xTraceQueue = NULL;

void vTrace_Log(const char* fmt, const char* file, int line, ...){
    va_list ap;
    if(xTraceQueue != NULL)
    {
    	char * buffer = pvPortMalloc(LOG_BUFFER_SIZE);
    	if(buffer != NULL)
    	{
			memset(buffer,0,LOG_BUFFER_SIZE);
			snprintf(buffer, LOG_SIZE, "%lu %s:%d: \"", xTaskGetTickCount(), file, line);
			uint32_t headsize = strlen(buffer);
			if(headsize < LOG_BUFFER_SIZE )
			{
				va_start(ap,0);
				vsnprintf(buffer + headsize, LOG_BUFFER_SIZE - headsize - 2, fmt, ap);
				va_end(ap);
				strcat(buffer, "\"\r\n");
				if( xQueueSend( xTraceQueue, &buffer, 0 ) != pdPASS )
				{
					vPortFree( ( void * ) buffer );
				}

			}

    	}
    }
}


BaseType_t xTrace_Init(UBaseType_t uxQueueLength )
{
    BaseType_t xReturn = pdFAIL;
	xTraceQueue = xQueueCreate( uxQueueLength, sizeof( char ** ) );
	if( xTraceQueue != NULL )
	{
		xReturn = xDebug_Init();
	}
    return xReturn;
}

void vTrace_Process(void)
{
    char * pcReceivedString = NULL;
	if( xQueueReceive( xTraceQueue, &pcReceivedString, portMAX_DELAY ) == pdPASS )
	{
		xDebug_Send((uint8_t *)pcReceivedString,strlen(pcReceivedString),TRACE_TIMEOUT);
		vPortFree( ( void * ) pcReceivedString );
	}
}

void vTrace_FailurePrint(void)
{
	xDebug_Send((uint8_t *)traceCriticalMessage,sizeof(traceCriticalMessage),TRACE_TIMEOUT);
}

void vTrace_LedOff(void)
{
	vDebug_SetLed(GPIO_PIN_SET);
}

void vTrace_LedON(void)
{
	vDebug_SetLed(GPIO_PIN_RESET);
}


