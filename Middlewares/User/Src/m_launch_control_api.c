/*
 * m_launch_control.c
 *
 *  Created on: 4Apr.,2020
 *      Author: skill
 */
#include "m_launch_control_api.h"
#include "m_trace_api.h"

#include "d_ecu_control.h"
#include "d_launch_control.h"


BaseType_t xLaunchControl_Init(void)
{
	if(xEcuControl_DriverInit() != pdPASS)
	{
		LOG("Failed to start ECU driver.");
		return pdFAIL;
	}else if(xLaunchControl_DriverInit() != pdPASS)
	{
		LOG("Failed to start launch control driver.");
		return pdFAIL;
	}
	return pdPASS;
}

BaseType_t xLaunchControl_IsActive()
{
	return xLaunchControl_PinActive();
}
