/*
 * m_apexi_echo.c
 *
 *  Created on: 4Apr.,2020
 *      Author: skill
 */
#include "a_main_task.h"
#include "m_hand_control_api.h"
#include "m_trace_api.h"
#include "d_ecu_control.h"
#include "d_hand_control.h"

BaseType_t xHandControl_Init()
{
	vEcuControl_DisablePort();
	return pdPASS;
}

void vHandControl_Enable()
{
	vHandControl_SetGPIO(GPIO_PIN_RESET);
	vEcuControl_DisablePort();
}

void vHandControl_Disable()
{
	vHandControl_SetGPIO(GPIO_PIN_SET);
	vEcuControl_EnablePort();
}
