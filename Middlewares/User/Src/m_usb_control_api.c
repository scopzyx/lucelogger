/*
 * m_usb_api.c
 *
 *  Created on: 4Apr.,2020
 *      Author: skill
 */

#include "string.h"

#include "a_settings.h"
#include "m_usb_control_api.h"
#include "m_trace_api.h"
#include "d_ecu_control.h"
#include "d_usb_control.h"
#include "m_usb_filter_list.h"

static uint8_t ecuCtrlBuff[MAX_PACKET_SIZE] = {0};
static uint8_t usbCtrlBuff[MAX_PACKET_SIZE] = {0};

static BaseType_t prvCheckFilterList(uint8_t * buff, uint16_t recBytes,uint32_t timeout);
BaseType_t xUsbControl_Init()
{
	if(xEcuControl_DriverInit() != pdPASS)
	{
		LOG("Failed to start ECU driver.");
		return pdFAIL;
	}else if(xUsbControl_DriverInit() != pdPASS)
	{
		LOG("Failed to start USB driver.");
		return pdFAIL;
	}
	return pdPASS;
}

void vUsbControl_SendRecv(uint32_t timeout)
{
	uint16_t recBytes;
	//Receive message from USB send to ECU.
	recBytes = xUsbControl_DriverRecv(usbCtrlBuff,sizeof(usbCtrlBuff),NO_WAIT);
	if(recBytes > 0)
	{
		if(pdFALSE == prvCheckFilterList(usbCtrlBuff,recBytes,timeout))
		{
			xEcuControl_DriverSend(usbCtrlBuff,recBytes,timeout);
		}
	}
	//Receive message from ECU, send to USB;
	recBytes = xEcuControl_DriverRecv(ecuCtrlBuff,sizeof(ecuCtrlBuff),NO_WAIT);
	if(recBytes > 0)
	{
		xUsbControl_DriverSend(ecuCtrlBuff,recBytes,timeout);
	}

}

BaseType_t xUsbControl_IsActive(void)
{
	return xUsbControl_IsConnected();
}

static BaseType_t prvCheckFilterList(uint8_t * buff, uint16_t recBytes,uint32_t timeout)
{
	for(uint8_t i = 0; i < (sizeof(filterList)/sizeof(filterItem_t));i++)
	{
		if(recBytes == filterList[i].rxItem.size)
		{
			if(!memcmp(buff,filterList[i].rxItem.bytes,recBytes))
			{
				xUsbControl_DriverSend((uint8_t *)filterList[i].txItem.bytes,filterList[i].txItem.size,timeout);
				return pdTRUE;
			}
		}
	}
	return pdFALSE;
}

