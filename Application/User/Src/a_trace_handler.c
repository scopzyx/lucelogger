/**
*	@file:		a_trace_handler.c
*	@project:	TrackSense 6A
*	@brief:		
*	@note:		
*	@date:		27Mar.,2020
*	@version:	1.0
*	@author:	Alex Williams
*	@attention: COPYRIGHT 2020 MRD Rail Technologies
*
*/
#include "a_trace_handler.h"
#include "a_settings.h"
#include "m_trace_api.h"
#include "cmsis_os.h"

void vTraceTask(void const * argument)
{
	if(xTrace_Init(TRACE_QUEUE_LEN) == pdPASS)
	{
		LOG("Trace Handler Started.");
		while(1)
		{
			vTrace_Process();
		}
	}
	vTrace_FailurePrint();
    vTaskDelete(NULL);
}

