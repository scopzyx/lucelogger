/**
*	@file:		a_task_handler.c
*	@project:	TrackSense 6A
*	@brief:		
*	@note:		
*	@date:		27Mar.,2020
*	@version:	1.0
*	@author:	Alex Williams
*	@attention: COPYRIGHT 2020 MRD Rail Technologies
*
*/
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "usb_device.h"

#include "a_task_handler.h"
#include "a_trace_handler.h"
#include "a_main_task.h"
#include "a_settings.h"

osThreadId mainTaskHandle;
osThreadId traceTaskHandle;

void vTaskInit(void const * argument)
{
	MX_USB_DEVICE_Init();

#if TRACE_TASK
    osThreadDef(TraceTask, vTraceTask, osPriorityIdle, 0, 128);
    traceTaskHandle = osThreadCreate(osThread(TraceTask), NULL);
#endif

#if MAIN_TASK
	osThreadDef(MainTask, vMainTask, osPriorityIdle, 0, 128);
	mainTaskHandle = osThreadCreate(osThread(MainTask), NULL);
#endif

    vTaskDelete(NULL);
}
