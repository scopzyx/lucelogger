/*
 * a_usb_task.c
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */
#include "a_usb_control_task.h"
#include "m_usb_control_api.h"
#include "m_hand_control_api.h"

void vUsbTask_Process(uint32_t timeout)
{
	vHandControl_Disable();
	vUsbControl_SendRecv(timeout);
}


