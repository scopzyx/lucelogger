/*
 * a_main_task.c
 *
 *  Created on: 4Apr.,2020
 *      Author: skill
 */
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"

#include "a_main_task.h"
#include "a_launch_control_task.h"
#include "a_usb_control_task.h"
#include "a_hand_control_task.h"
#include "a_settings.h"

#include "m_trace_api.h"
#include "m_usb_control_api.h"
#include "m_launch_control_api.h"
#include "m_hand_control_api.h"

void vMainTask(void const * argument)
{
	BaseType_t usbSetup = pdFAIL;
	BaseType_t handCtrlSetup = pdFAIL;
	BaseType_t launchCtrlSetup = pdFAIL;
	LOG("Main Task Started.");
#if USB_CONTROL
	usbSetup = xUsbControl_Init();
	if(usbSetup != pdPASS) LOG("USB control failed to initialize.");
#endif
#if LAUNCH_CONTROL
	launchCtrlSetup = xLaunchControl_Init();
	if(launchCtrlSetup != pdPASS) LOG("Launch control failed to initialize.");
#endif
#if HAND_CONTROL
	handCtrlSetup = xHandControl_Init();
	if(handCtrlSetup != pdPASS) LOG("Hand control failed to initialize.");
#endif
	while(1)
	{
		if(pdPASS == usbSetup && pdPASS == xUsbControl_IsActive())
		{
			vUsbTask_Process(TASK_TIMEOUT);
		}else if(pdPASS == launchCtrlSetup && pdPASS == xLaunchControl_IsActive())
		{
			vLaunchControl_Process(TASK_TIMEOUT);
		}
		else
		{
			vHandControl_Process(TASK_TIMEOUT);
		}
	}
}

