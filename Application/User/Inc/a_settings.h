/*
 * a_settings.h
 *
 *  Created on: 12Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_A_SETTINGS_H_
#define USER_INC_A_SETTINGS_H_

/**********  Task Settings  **********/
#define MAIN_TASK					1		//Default task handler
#define TRACE_TASK					0		//Debug Trace Task

/**********  Function Settings  **********/
#define USB_CONTROL				1
#define LAUNCH_CONTROL			1
#define	HAND_CONTROL			1

/**********  Configuration Settings  **********/
#define LAUNCH_DEBOUNCE_TIME 50				//Time in ms to trigger launch active/de-active.
#define MAX_PACKET_SIZE 1024				//Packet buffer size for UART/USB.
#define NO_WAIT 0							//No wait time.
#define TRACE_QUEUE_LEN 10					//Number of trace messages to store in queue.
#define TASK_TIMEOUT 1000 					//Main task function timeout.

#endif /* USER_INC_A_SETTINGS_H_ */
