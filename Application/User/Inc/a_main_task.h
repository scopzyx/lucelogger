/*
 * a_main_task.h
 *
 *  Created on: 4Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_A_MAIN_TASK_H_
#define USER_INC_A_MAIN_TASK_H_

void vMainTask(void const * argument);


#endif /* USER_INC_A_MAIN_TASK_H_ */
