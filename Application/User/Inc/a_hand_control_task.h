/*
 * a_hand_control_task.h
 *
 *  Created on: 12Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_A_HAND_CONTROL_TASK_H_
#define USER_INC_A_HAND_CONTROL_TASK_H_
#include "FreeRTOS.h"

void vHandControl_Process(uint32_t timeout);

#endif /* USER_INC_A_HAND_CONTROL_TASK_H_ */
