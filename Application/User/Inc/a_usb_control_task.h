/*
 * a_usb_task.h
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_A_USB_CONTROL_TASK_H_
#define USER_INC_A_USB_CONTROL_TASK_H_
#include "FreeRTOS.h"
void vUsbTask_Process(uint32_t timeout);

#endif /* USER_INC_A_USB_CONTROL_TASK_H_ */
