/*
 * a_launch_control_task.h
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_A_LAUNCH_CONTROL_TASK_H_
#define USER_INC_A_LAUNCH_CONTROL_TASK_H_
#include "FreeRTOS.h"

void vLaunchControl_Process(uint32_t timeout);

#endif /* USER_INC_A_LAUNCH_CONTROL_TASK_H_ */
