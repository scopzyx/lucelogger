/**
*	@file:		a_task_handler.h
*	@project:	TrackSense 6A
*	@brief:		
*	@note:		
*	@date:		27Mar.,2020
*	@version:	1.0
*	@author:	Alex Williams
*	@attention: COPYRIGHT 2020 MRD Rail Technologies
*
*/

#ifndef MRD_INC_A_TASK_HANDLER_H_
#define MRD_INC_A_TASK_HANDLER_H_

void vTaskInit(void const * argument);

#endif /* MRD_INC_A_TASK_HANDLER_H_ */
