/**
*	@file:		a_trace_handler.h
*	@project:	TrackSense 6A
*	@brief:		
*	@note:		
*	@date:		27Mar.,2020
*	@version:	1.0
*	@author:	Alex Williams
*	@attention: COPYRIGHT 2020 MRD Rail Technologies
*
*/

#ifndef MRD_INC_A_TRACE_HANDLER_H_
#define MRD_INC_A_TRACE_HANDLER_H_

void vTraceTask(void const * argument);

#endif /* MRD_INC_A_TRACE_HANDLER_H_ */
