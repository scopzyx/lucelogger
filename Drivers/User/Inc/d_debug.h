/*
 * d_debug.h
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_D_DEBUG_H_
#define USER_INC_D_DEBUG_H_
#include "FreeRTOS.h"
#include "gpio.h"
BaseType_t xDebug_Init(void);
BaseType_t xDebug_Send(uint8_t * data,uint16_t dataSize,uint32_t timeout);
void vDebug_TogglePin(void);
void vDebug_ToggleLed(void);
void vDebug_SetLed(GPIO_PinState PinState);

#endif /* USER_INC_D_DEBUG_H_ */
