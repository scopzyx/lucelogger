/*
 * d_hand_control.h
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_D_HAND_CONTROL_H_
#define USER_INC_D_HAND_CONTROL_H_
#include "gpio.h"
void vHandControl_SetGPIO(GPIO_PinState PinState);

#endif /* USER_INC_D_HAND_CONTROL_H_ */
