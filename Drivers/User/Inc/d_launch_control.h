/*
 * d_launch_control.h
 *
 *  Created on: 12Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_D_LAUNCH_CONTROL_H_
#define USER_INC_D_LAUNCH_CONTROL_H_
#include "FreeRTOS.h"


BaseType_t xLaunchControl_DriverInit(void);
BaseType_t xLaunchControl_PinActive(void);

#endif /* USER_INC_D_LAUNCH_CONTROL_H_ */
