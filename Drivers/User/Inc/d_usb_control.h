/*
 * d_usb_control.h
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_D_USB_CONTROL_H_
#define USER_INC_D_USB_CONTROL_H_
#include "FreeRTOS.h"

BaseType_t xUsbControl_DriverInit(void);
BaseType_t xUsbControl_DriverSend(uint8_t * data, uint16_t dataSize, uint32_t timeout);
uint16_t xUsbControl_DriverRecv(uint8_t * data, uint16_t dataSize, uint32_t timeout);
BaseType_t xUsbControl_IsConnected(void);

void xUsbControl_RxCpltCallback(uint8_t * rxData, uint16_t rxSize);

#endif /* USER_INC_D_USB_CONTROL_H_ */
