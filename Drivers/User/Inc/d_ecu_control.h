/*
 * d_ecu.h
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */

#ifndef USER_INC_D_ECU_CONTROL_H_
#define USER_INC_D_ECU_CONTROL_H_
#include "FreeRTOS.h"
#include "usart.h"

#define ECU_CONTROL_UART huart2

BaseType_t xEcuControl_DriverInit(void);
BaseType_t xEcuControl_DriverSend(uint8_t * data, uint16_t dataSize, uint32_t timeout);
uint16_t xEcuControl_DriverRecv(uint8_t * data, uint16_t dataSize, uint32_t timeout);
void vEcuControl_DisablePort(void);
void vEcuControl_EnablePort(void);


void vEcuControl_TxCpltCallback(UART_HandleTypeDef *UartHandle);
void vEcuControl_RxCpltCallback(UART_HandleTypeDef *UartHandle);
void vEcuControl_IRQCallback(UART_HandleTypeDef *UartHandle);

#endif /* USER_INC_D_ECU_CONTROL_H_ */
