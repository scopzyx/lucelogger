/*
 * d_usb_control.c
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */

#include "cmsis_os.h"
#include "string.h"
#include "usbd_cdc_if.h"

#include "a_settings.h"
#include "d_usb_control.h"
#include "usb_device.h"

#define RX_QUEUE_SIZE 40

typedef struct Usb_Packet_t
{
	uint8_t PacketBuffer[USB_FS_MAX_PACKET_SIZE];
	uint8_t PacketSize;
}Usb_Packet_t;

static osMessageQId USBRxMessage = NULL;
static Usb_Packet_t USBRxBuffer[RX_QUEUE_SIZE] = {0};
static volatile uint32_t RxBufferCount = 0;
static volatile uint32_t RequestedRxCount = 0;
static volatile uint32_t ProcessedCount = 0;
BaseType_t prvRestartRx(void);

BaseType_t xUsbControl_DriverInit(void)
{
	if(USBRxMessage == NULL)
	{
		osMessageQDef(USBRxMessage, RX_QUEUE_SIZE, uint16_t);
		USBRxMessage = osMessageCreate (osMessageQ(USBRxMessage), NULL);
	}
	return prvRestartRx();
}

BaseType_t xUsbControl_DriverSend(uint8_t * data, uint16_t dataSize, uint32_t timeout)
{
	if(USBD_OK != CDC_Transmit_FS(data, dataSize))
	{
		return pdFAIL;
	}
	return pdPASS;
}

uint16_t xUsbControl_DriverRecv(uint8_t * data, uint16_t dataSize, uint32_t timeout)
{
	osEvent event = {0};
	uint16_t rxSize = 0;
	event = osMessagePeek(USBRxMessage,timeout);
	while(event.status == osEventMessage)
	{
		uint8_t index = event.value.v;
		if(index < RX_QUEUE_SIZE)
		{
			uint16_t packetSize = USBRxBuffer[index].PacketSize;
			if((rxSize+packetSize) > dataSize)
			{
				return rxSize;
			}
			memcpy(data + rxSize,USBRxBuffer[index].PacketBuffer,packetSize);
			rxSize += packetSize;
			USBRxBuffer[index].PacketSize = 0;
			osMessageGet(USBRxMessage,timeout);
		}
		event = osMessagePeek(USBRxMessage,timeout);
	}
	return rxSize;
}

BaseType_t prvRestartRx(void)
{
	return pdPASS;
}

BaseType_t xUsbControl_IsConnected(void)
{
	return xUSBD_LL_IsConnected();
}

void xUsbControl_RxCpltCallback(uint8_t * rxData, uint16_t rxSize)
{
	if(USBRxMessage != NULL)
	{
		if(rxSize > USB_FS_MAX_PACKET_SIZE)
		{
			rxSize = USB_FS_MAX_PACKET_SIZE;
		}
		for(uint8_t i = 0; i < RX_QUEUE_SIZE; i++)
		{
			if(USBRxBuffer[i].PacketSize == 0)
			{
				memcpy(USBRxBuffer[i].PacketBuffer,rxData,rxSize);
				USBRxBuffer[i].PacketSize = rxSize;
				osMessagePut(USBRxMessage,i,NO_WAIT);
				return;
			}
		}

	}
}
