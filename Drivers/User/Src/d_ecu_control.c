/*
 * d_ecu.c
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */
#include "a_settings.h"
#include "d_ecu_control.h"
#include "cmsis_os.h"
#include "string.h"
#include "gpio.h"


static osMessageQId TxMessage = NULL;
static osMessageQId RxMessage = NULL;
static uint8_t RxBuffer[MAX_PACKET_SIZE] = {0};
static GPIO_InitTypeDef GPIO_InitStruct = {0};

static void prvRestartRx(void);

BaseType_t xEcuControl_DriverInit(void)
{
	if(TxMessage == NULL || RxMessage == NULL)
	{
		osMessageQDef(TxMessage, 1, uint16_t);
		TxMessage = osMessageCreate (osMessageQ(TxMessage), NULL);

		osMessageQDef(RxMessage, 1, uint16_t);
		RxMessage = osMessageCreate (osMessageQ(RxMessage), NULL);
	}
	__HAL_UART_ENABLE_IT(&ECU_CONTROL_UART, UART_IT_IDLE);
	prvRestartRx();
	return pdPASS;
}

void vEcuControl_DisablePort(void)
{
	if(GPIO_InitStruct.Mode != GPIO_MODE_OUTPUT_PP)
	{
	    GPIO_InitStruct.Pin = GPIO_PIN_2;
	    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_RESET);
	}
}

void vEcuControl_EnablePort(void)
{
	if(GPIO_InitStruct.Mode != GPIO_MODE_AF_PP)
	{
	    GPIO_InitStruct.Pin = GPIO_PIN_2;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	}
}

BaseType_t xEcuControl_DriverSend(uint8_t * data, uint16_t dataSize, uint32_t timeout)
{
	osEvent event;
    HAL_UART_Transmit_DMA(&ECU_CONTROL_UART, data, dataSize);
	event = osMessageGet(TxMessage,timeout);
	if(event.status == osEventMessage)
	{
		return pdPASS;
	}
	return pdFAIL;
}

uint16_t xEcuControl_DriverRecv(uint8_t * data, uint16_t dataSize, uint32_t timeout)
{
	osEvent event;
	event = osMessageGet(RxMessage,timeout);
	uint16_t rxSize = 0;
	if(event.status == osEventMessage)
	{
		rxSize = ECU_CONTROL_UART.RxXferSize -  ECU_CONTROL_UART.hdmarx->Instance->CNDTR;
		if(rxSize > dataSize) rxSize =  dataSize;
		memcpy(data,RxBuffer,rxSize);
		prvRestartRx();
	}
	return rxSize;
}

void prvRestartRx(void)
{
	HAL_UART_DMAStop(&ECU_CONTROL_UART);
	memset(RxBuffer,0,sizeof(RxBuffer));
	HAL_UART_Receive_DMA(&ECU_CONTROL_UART, RxBuffer, sizeof(RxBuffer));
}

void vEcuControl_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	if(UartHandle == &ECU_CONTROL_UART && TxMessage != NULL)
	{
		osMessagePut(TxMessage,pdPASS,NO_WAIT);
	}
}
void vEcuControl_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	if(UartHandle == &ECU_CONTROL_UART && RxMessage != NULL)
	{
		osMessagePut(RxMessage,pdPASS,NO_WAIT);
	}
}
void vEcuControl_IRQCallback(UART_HandleTypeDef *UartHandle)
{
	if(UartHandle == &ECU_CONTROL_UART)
	{
		  if (__HAL_UART_GET_FLAG(UartHandle,UART_FLAG_IDLE) && __HAL_UART_GET_IT_SOURCE(UartHandle,UART_IT_IDLE)) {
			  __HAL_UART_CLEAR_IDLEFLAG(UartHandle);
			  osMessagePut(RxMessage,pdPASS,NO_WAIT);
		  }
	}
}
