/*
 * d_debug.c
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */
#include "main.h"
#include "d_debug.h"


BaseType_t xDebug_Init(void)
{
	return pdPASS;
}
BaseType_t xDebug_Send(uint8_t * data,uint16_t dataSize,uint32_t timeout)
{
	uint32_t tick = HAL_GetTick();
	for(uint16_t i = 0; i < dataSize; i++)
	{
		ITM_SendChar(*data++);
		if((HAL_GetTick() - tick)  > timeout) return pdFAIL;
	}
	return pdPASS;
}

void vDebug_TogglePin(void)
{
	HAL_GPIO_TogglePin(DBG_IO_GPIO_Port,DBG_IO_Pin);
}

void vDebug_ToggleLed(void)
{
	HAL_GPIO_TogglePin(LED1_GPIO_Port,LED1_Pin);
}

void vDebug_SetLed(GPIO_PinState PinState)
{
	HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,PinState);
}
