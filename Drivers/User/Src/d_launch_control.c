/*
 * d_launch_control.c
 *
 *  Created on: 12Apr.,2020
 *      Author: skill
 */
#include "a_settings.h"
#include "d_launch_control.h"
#include "task.h"
#include "gpio.h"

static volatile GPIO_PinState currentState = GPIO_PIN_RESET;
static volatile GPIO_PinState defaultState = GPIO_PIN_RESET;
static volatile TickType_t triggerTime = portMAX_DELAY;

BaseType_t xLaunchControl_DriverInit(void)
{
	//Current State
	TickType_t currTime = xTaskGetTickCount();
	defaultState = HAL_GPIO_ReadPin(LAUNCH_IN_GPIO_Port,LAUNCH_IN_Pin);

	//Make sure the pin stays stable for atleast the debounce time.
	while((xTaskGetTickCount() - currTime) <= LAUNCH_DEBOUNCE_TIME)
	{
		if(HAL_GPIO_ReadPin(LAUNCH_IN_GPIO_Port,LAUNCH_IN_Pin) != defaultState)
		{
			defaultState = HAL_GPIO_ReadPin(LAUNCH_IN_GPIO_Port,LAUNCH_IN_Pin);
			currTime = xTaskGetTickCount();
		}
	}
	currentState = defaultState;
	return pdPASS;
}

BaseType_t xLaunchControl_PinActive(void)
{
	//Has the pin changed.
	if(HAL_GPIO_ReadPin(LAUNCH_IN_GPIO_Port,LAUNCH_IN_Pin) != currentState)
	{
		//Check if its been that way for longer then the debounce time.
		if((xTaskGetTickCount() - triggerTime) >= LAUNCH_DEBOUNCE_TIME)
		{
			currentState = HAL_GPIO_ReadPin(LAUNCH_IN_GPIO_Port,LAUNCH_IN_Pin);
		}
	}
	else
	{
		triggerTime = xTaskGetTickCount();
	}

	//Return true and false.
	if(defaultState != currentState) return pdTRUE;
	return pdFALSE;
}

