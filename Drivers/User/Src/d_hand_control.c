/*
 * d_hand_control.c
 *
 *  Created on: 5Apr.,2020
 *      Author: skill
 */

#include "d_hand_control.h"

void vHandControl_SetGPIO(GPIO_PinState PinState)
{
	HAL_GPIO_WritePin(COMMANDER_EN_RX_GPIO_Port,COMMANDER_EN_RX_Pin,PinState);
	HAL_GPIO_WritePin(COMMANDER_EN_TX_GPIO_Port,COMMANDER_EN_TX_Pin,!PinState);
}
